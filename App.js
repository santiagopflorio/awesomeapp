/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';

import {Root} from './src/configurations/Root';

export default class App extends Component {
  render() {
    return <Root />;
  }
}
