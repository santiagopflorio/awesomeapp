import {put, all, takeLatest, call} from 'redux-saga/effects';
import {getMovies} from '../api/movies';
import {saveMovies} from '../actions/MoviesActions';
import {GET_MOVIES_SAGA} from '../actions/ActionTypes';

function* searchMovies(action) {
  const {name} = action.payload;
  let movies = yield call(getMovies, name);
  yield put(saveMovies(movies));
}

const sagas = [takeLatest(GET_MOVIES_SAGA, searchMovies)];

export default function* sagasRoot() {
  yield all([...sagas]);
}
