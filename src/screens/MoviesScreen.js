import React, {useState} from 'react';

import {SearchBar} from 'react-native-elements';
import MovieInfoComponent from '../components/MovieInfoComponent';
import {View, FlatList} from 'react-native';

const MoviesScreen = (props) => {
  const {movies, callback} = props;
  const [search] = useState(null);

  const onTextChange = (event) => {
    callback(event.nativeEvent.text);
  };

  function renderItem({item}) {
    return <MovieInfoComponent movie={item} />;
  }

  return (
    <View>
      <SearchBar
        placeholder="Type Here..."
        onEndEditing={onTextChange}
        value={search}
      />
      <FlatList
        data={movies}
        keyExtractor={(item, index) => {
          return index.toString();
        }}
        renderItem={renderItem}
        numColumns={1}
      />
    </View>
  );
};

export default MoviesScreen;
