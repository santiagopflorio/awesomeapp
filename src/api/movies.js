import {get} from './imdb8.api';

class MovieImage {
  height: number;
  imageUrl: string;
  width: number;
}

class Movie {
  id: string;
  l: string;
  q: string;
  i: MovieImage;
}

class Movies {
  d: [Movie] = [];
}

export const getMovies = async (name: string) => {
  return get('title/auto-complete', {q: name}).then((result) => {
    let movies: Movies = result.data;
    return movies;
  });
};
