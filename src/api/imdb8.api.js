import axios from 'axios';
import {API_URL, API_KEY} from '../../envs/imdb8.json';

axios.defaults.baseURL = API_URL;
axios.defaults.headers.post.Accept = 'application/json';
axios.defaults.headers.post['Content-Type'] = 'application/json';
axios.defaults.timeout = 10000;

const parseError = (error) => {
  const errorResponse = {
    fail: true,
  };
  if (error.response) {
    // The request was made and the server responded with a status code
    // that falls out of the range of 2xx
    errorResponse.status = error.response.status;
    errorResponse.message = error;
  } else if (error.request) {
    // The request was made but no response was received
    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
    errorResponse.message = error;
  } else {
    // Something happened in setting up the request that triggered an Error
    errorResponse.message = error;
  }

  return errorResponse;
};

const headers = {
  'x-rapidapi-host': 'imdb8.p.rapidapi.com',
  'x-rapidapi-key': '76b821d9abmshe4ebf307c951a20p16678djsn5690c36d88fe',
  useQueryString: true,
};

export const get = async (url, params?: any) => {
  let config = {
    headers: headers,
    params: params,
  };

  return axios.get(url, config).catch((error) => {
    return parseError(error);
  });
};
