import React, {useEffect} from 'react';

import {connect} from 'react-redux';
import MoviesScreen from '../screens/MoviesScreen';
import PropTypes from 'prop-types';
import {getMovies} from '../actions/MoviesActions';

const MoviesContainer = (props) => {
  const {movies, getMovies} = props;

  const searchCallback = (name) => {
    getMovies(name);
  };

  return <MoviesScreen movies={movies} callback={searchCallback} />;
};

const mapStateToProps = (state) => {
  return {
    movies: state.movies.d,
  };
};

MoviesContainer.protoTypes = {
  movies: PropTypes.array,
  getMovies: PropTypes.func,
};

const mapDispatch = {
  getMovies,
};

export default connect(mapStateToProps, mapDispatch)(MoviesContainer);
