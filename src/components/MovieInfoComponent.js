import React from 'react';

import {View, Image, Text, ImageBackground} from 'react-native';
import {styles} from './MovieInfoStylesheet';

import PropTypes from 'prop-types';

const MovieInfoComponent = (props) => {
  const {movie} = props;

  return (
    <View style={styles.container}>
      <ImageBackground resizeMethod={'scale'} style={styles.image} source={{uri: movie.i ? movie.i.imageUrl : 'https://picsum.photos/seed/picsum/200/300'}}>
        <Text style={styles.title} numberOfLines={1} ellipsizeMode={'tail'}>{movie.l || 'Not found'}</Text>
      </ImageBackground>
    </View>
  );
};

MovieInfoComponent.protoTypes = {
  movie: PropTypes.object,
};

export default MovieInfoComponent;
