import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    height: 200,
    paddingTop: 2.5,
    backgroundColor: 'grey',
  },
  image: {
    flex: 1,
    resizeMode: 'cover',
  },
  title: {
    position: 'absolute',
    left: 5,
    top: 5,
    fontWeight: 'bold',
    color: 'white',
    fontSize: 12,
    backgroundColor: 'orange',
    borderRadius: 20,
    padding: 5,
  },
});
