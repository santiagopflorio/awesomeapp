import {createStore, applyMiddleware} from 'redux';
import createSagaMiddleware from 'redux-saga';

import {Provider} from 'react-redux';

import reducers from '../reducers';

import sagasRoot from '../sagas';

import React from 'react';
import {SafeAreaView} from 'react-native';
import MoviesContainer from '../containers/MoviesContainer';

export const Root = () => {
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(reducers, applyMiddleware(sagaMiddleware));

  sagaMiddleware.run(sagasRoot);

  return (
    <SafeAreaView style={{flex: 1}}>
      <Provider store={store}>
        <MoviesContainer />
      </Provider>
    </SafeAreaView>
  );
};
