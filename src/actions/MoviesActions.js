import {GET_MOVIES_SAGA, SAVE_MOVIES_REDUCER} from './ActionTypes';

export function getMovies(name) {
  return {
    type: GET_MOVIES_SAGA,
    payload: {
      name: name,
    },
  };
}

export function saveMovies(movies) {
  return {
    type: SAVE_MOVIES_REDUCER,
    payload: movies,
  };
}
