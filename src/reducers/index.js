import {SAVE_MOVIES_REDUCER} from '../actions/ActionTypes';

const initialState = {
  movies: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
    case SAVE_MOVIES_REDUCER:
      return {
        ...state,
        movies: action.payload,
      };
    default:
      return initialState;
  }
}
